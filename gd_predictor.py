from tools import utils
import sys
import time
import threading
import redis
import traceback
import numpy as np
from arp.common import redis_utils
import settings
import datetime
from arp.preprocessor import excalibur_preprocessor
from pymongo import MongoClient
from arp.common import mongo_utils
from gd_proprocess import preprocess
import tensorflow as tf
from keras import backend as K
from keras.backend.tensorflow_backend import set_session

class gd_predict:
    def __init__(self):
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.3
        # config.gpu_options.per_process_gpu_memory_fraction = 0.5

        K.tensorflow_backend.manual_variable_initialization(False)

        # Initialize all variables
        sess = tf.Session(config=config)
        set_session(sess)


        # regress = False
        self.max_ratio_list = []
        self.min_ratio_list = []
        self.regress = True
        self.win_percent = 0
        self.profit_ratio = 0
        self.max_dd = 0
        self.reward = 0
        # self.win_percent_queue = Queue()
        # self.profit_ratio_queue = Queue()
        # self.max_dd_queue = Queue()
        # self.reward_queue = Queue()
        self.ask_model_success_rate = Queue()
        self.ask_model_margin_avg = Queue()
        self.ask_model_expected_profit = Queue()
        self.bid_model_success_rate = Queue()
        self.bid_model_margin_avg = Queue()
        self.bid_model_expected_profit = Queue()
        self.pred_prob = []


if __name__=='__main__':

    # Get parameters
    argv = sys.argv
    print(argv)

    # if len(argv) < 2:
    #     settings.LOGGER.info('usage: python gd_predictor.py [filename]')
    #     exit()
    #
    # filename = argv[1]
    filename = '/home/rtm/jupyter/data/GD/2019040805'

    print(filename)
    # create instance
    pred_inst = gd_predict()

    # read pre-processed data

    pred_inst.feature_set, pred_inst.feature_name_set, pred_inst.max_ratio_list, pred_inst.min_ratio_list = preprocess(filename)


    redis_client = redis.StrictRedis(host=settings.REDIS_HOST, port=6379, db=0)

    client = MongoClient('gpu.rtm.ai', port=14350, username='rtmadmin', password='Rocket!Go!go@')
    mongo_db = client['arp']

    # Get parameters
    argv = sys.argv

    print(argv)

    if len(argv) < 2:
        settings.LOGGER.info('usage: python trade_status_updater.py [model_version]')
        exit()

    model_version = argv[1]
    # window_mode = argv[2]

    # TODO: Trade Mode를 받기 (읽어드릴 윈도우, 실제 트레이드할 대상 선택할 수 있도록)

    '''
    Wait for loading (lazy prediction)
    - 모델 로딩에 약 20초 소요
    - 아예 최신 데이터를 예측하기 위하여 30초 쉬고 로딩하기 시작하여 다음 분의 예측치를 이용하기
    '''
    # if window_mode != 'main':
    #     time.sleep(30)

    # Load Model
    # model_path = settings.BASE_PATH + 'model/arp/' + model_version + '/coin-trend-cnn-regress-best.hdf5'
    model_path = settings.BASE_PATH + 'model/arp/' + model_version + '/excalibur-bitmex-classifier-best.hdf5'
    model = load_trend_model(model_path)

    # symbols = ['BTC', 'ETH', 'XRP', 'BCH', 'EOS', 'MIOTA']
    # symbols += ['QTUM', 'ZIL']
    # symbols += ['ETC', 'LTC', 'OMG', 'XTZ', 'ZRX']

    # symbols = settings.PRED_SYMBOLS


    idx = 0

    for param in settings.EXCALIBUR_PREDICTOR_PARAMS:

        target_exchange = param['target_exchange']
        symbol_pair = param['symbol_pair']
        data_type = param['data_type']
        interval = param['interval']
        max_len = param['max_len']

        try:

            # Create new threads
            thread = PredictorThread(idx, redis_client, mongo_db, model, target_exchange, symbol_pair, data_type, interval, max_len)

            # Start new Threads
            thread.start()

            idx += 1
        except:
            settings.LOGGER.error('[ERROR] unable to start thread: %s', traceback.format_exc().replace('\n', ' '))
