from keras.layers import Input, Dense, Dropout, Conv1D, MaxPooling1D, Flatten, \
    Concatenate, Permute, Reshape, Lambda, RepeatVector, Multiply, BatchNormalization, Activation, CuDNNGRU, GRU
from keras.models import Model
from keras import optimizers
from keras.callbacks import ModelCheckpoint, EarlyStopping
import numpy as np
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
from keras import backend as K
import settings
import os
from keras.models import load_model
from ml.keras_custom import relu_squre, rmse, polarity_accuracy, profit_backtesting, cosine_sim, pos_cnt, \
    neg_cnt, cosine_loss, pred_std
from ml.keras_custom import rmse, polarity_accuracy, profit_backtesting, cosine_sim, pos_cnt, \
    neg_cnt, cosine_loss, pred_std, precision, recall, f1score

import numpy as np

from keras.utils import Sequence
import pymongo
from arp.common import mongo_utils

from random import shuffle
from pymongo import MongoClient

import datetime
import sys
import time

import json

import glob

import keras
from gd_proprocess import preprocess

def _load_data(batch_file):
    npy_path_ts_x = batch_file + '.ts_x.npy'
    npy_path_flat_x = batch_file + '.flat_x.npy'
    npy_path_y = batch_file + '.y.npy'

    if os.path.exists(npy_path_y):
        # print('use npy...')
        # npy가 있으면 불러오기
        timeseries_xs = np.load(npy_path_ts_x)
        flat_xs = np.load(npy_path_flat_x)
        ys = np.load(npy_path_y)
    else:
        # npy가 없으면 신규 적재
        timeseries_xs = []
        flat_xs = []
        ys = []

        with open(batch_file, 'r') as fr:
            for doc in fr:
                doc = json.loads(doc)

                # if doc['target'] is None:
                #     continue

                y = doc['target'][0]

                '''
                if y > 1.0:
                    # buy
                    y = [0, 1, 0]
                elif y < -1.0:
                    # sell
                    y = [0, 0, 1]
                else:
                    y = [1, 0, 0]

                '''

                # if y > 0.3:
                #     # buy
                #     y = [0, 1, 0]
                # elif y < -0.3:
                #     # sell
                #     y = [0, 0, 1]
                # elif y >= -0.2 and y <=0.2:
                #     y = [1, 0, 0]
                # else:
                #     continue

                if y > 0.05:
                    # buy
                    y = [0, 1, 0]
                elif y < -0.05:
                    # sell
                    y = [0, 0, 1]
                else:
                    # hold
                    y = [1, 0, 0]
                # else:
                #     continue

                '''
                elif y >= -0.2 and y <= 0.2:
                    # hold
                    y = [1, 0, 0]
                else:
                    # 애매한 바운더리는 날려보리기
                    continue
                '''
                ys.append(y)  # test

                # for doc in docs:
                timeseries_xs.append(doc['timeseries_feature'])
                flat_xs.append(doc['flat_feature'])
                # ys.append(doc['target'])

        # del self._collection  # to be sure, that we've disconnected

        # end_time = time.time()
        #
        # print('duration = ', end_time - start_time)

        timeseries_xs = np.array(timeseries_xs)
        flat_xs = np.array(flat_xs)
        ys = np.array(ys)

        # save npy data

        np.save(npy_path_ts_x, timeseries_xs)
        np.save(npy_path_flat_x, flat_xs)
        np.save(npy_path_y, ys)

        # npy 저장 후, batch file 제거
        os.remove(batch_file)

    return timeseries_xs, flat_xs, ys


class DataSequence(Sequence):
    def __init__(self, file_path):
        self.batch_list = glob.glob(file_path + '*')

    def __len__(self):
        return len(self.batch_list)

    def __getitem__(self, item):
        batch_file = self.batch_list[item]

        # print('batch_file = ', batch_file, item)

        timeseries_xs, flat_xs, ys = _load_data(batch_file)

        # end_time2 = time.time()
        #
        # print('duration2 = ', end_time2 - end_time)

        return [timeseries_xs, flat_xs], ys
        # return timeseries_xs, ys


# class MongoSequence(Sequence):
#     def __init__(self, object_ids, batch_size,
#                  source=None, symbol_pair=None, server=None, database=None, collection=None):
#         # self._train_set = train_set
#         self._collection_name = collection
#         self._batch_size = batch_size
#
#         # self.source = source
#         # self.symbol_pair = symbol_pair
#
#         self._server = server
#         self._db = database
#
#         # query = {'source': source, 'symbol_pair': symbol_pair, 'time_id': {'$gte': start_date, '$lte': end_date}}  # train_Set query
#         self._object_ids = object_ids
#
#         self._pid = os.getpid()
#         self._collection = None
#
#     def __len__(self):
#         return int(np.ceil(len(self._object_ids) / float(self._batch_size)))
#
#     def _connect(self):
#         self._client = pymongo.MongoClient(self._server)
#         db = self._client[self._db]
#         return db[self._collection_name]
#
#     def __getitem__(self, item):
#
#         if self._collection is None or self._pid != os.getpid():
#             self._collection = self._connect()
#             self._pid = os.getpid()
#
#         oids = self._object_ids[item * self._batch_size: (item+1) * self._batch_size]
#
#         # query = {'source': self.source, 'symbol_pair': self.symbol_pair, 'time_id': {'$in': oids}}
#         # docs = self._collection.find(query)
#
#         # X = np.empty((len(oids), IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS), dtype=np.float32)
#         # y = np.empty((len(oids), 2), dtype=np.float32)
#         # for i, oid in enumerate(oids):
#         #     smp = self._connect().find({'_id': oid}).next()
#         #     X[i, :, :, :] = pickle.loads(smp['frame']).astype(np.float32)
#         #     y[i] = to_categorical(not smp['result'], 2)
#
#
#         flat_xs = []
#         timeseries_xs = []
#         ys = []
#
#         # for doc in docs:
#
#         # start_time = time.time()
#         for oid in oids:
#             # query = {'source': self.source, 'symbol_pair': self.symbol_pair, 'time_id': oid}
#             query = {'_id': oid}
#             doc = self._collection.find(query, {'timeseries_feature': True, 'flat_feature': True, 'target': True, }).next()
#
#             # for doc in docs:
#             timeseries_xs.append(doc['timeseries_feature'])
#             flat_xs.append(doc['flat_feature'])
#             ys.append(doc['target'])
#
#         # del self._collection  # to be sure, that we've disconnected
#
#         # end_time = time.time()
#         #
#         # print('duration = ', end_time - start_time)
#
#         timeseries_xs = np.array(timeseries_xs)
#         flat_xs = np.array(flat_xs)
#         ys = np.array(ys)
#
#         # end_time2 = time.time()
#         #
#         # print('duration2 = ', end_time2 - end_time)
#
#         return [timeseries_xs, flat_xs], ys

SINGLE_ATTENTION_VECTOR = False


def attention_3d_block(inputs):
    input_dim = int(inputs.shape[2])
    a = Permute((2, 1))(inputs)
    a = Reshape((input_dim, max_len))(a)  # this line is not useful. It's just to know which dimension is what.
    a = Dense(max_len, activation='softmax', kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(a)
    if SINGLE_ATTENTION_VECTOR:
        a = Lambda(lambda x: K.mean(x, axis=1), name='dim_reduction')(a)
        a = RepeatVector(input_dim)(a)
    a_probs = Permute((2, 1), name='attention_vec')(a)
    output_attention_mul = Multiply(name='attention_mul')([inputs, a_probs])
    return output_attention_mul


def create_model(input_shape, flat_input_shape, dropout_prob, hidden_dims, num_filters, filter_sizes):
    activate_f = 'relu'

    # flat
    flat_input = Input(shape=flat_input_shape)

    flat_dense = Dense(int(flat_input_shape[0]),
                       kernel_initializer='glorot_normal',
                       bias_initializer='glorot_normal',
                       activation=activate_f,
                       name='flat_dense1')(flat_input)

    flat_attention_probs = Dense(int(flat_input_shape[0]), activation='softmax',
                                 kernel_initializer='glorot_normal',
                                 bias_initializer='glorot_normal',
                                 name='flat_dense_attention_probs')(flat_dense)

    flat_dense = Multiply(name='flat_dense_attention')([flat_dense, flat_attention_probs])

    flat_dense = BatchNormalization()(flat_dense)  # batch normalization
    flat_dense = Activation(activate_f)(flat_dense)

    # time series
    model_input = Input(shape=input_shape)

    z = model_input

    attention_mul = attention_3d_block(model_input)
    z = attention_mul
    z = Dropout(dropout_prob[0])(z)

    # Convolutional block
    conv_blocks = []
    sub_i = 0
    for sz in filter_sizes:
        conv = Conv1D(filters=num_filters,
                      kernel_size=sz,
                      padding="valid",
                      kernel_initializer='glorot_normal',
                      bias_initializer='glorot_normal',
                      activation=activate_f,
                      strides=1)(z)

        conv = MaxPooling1D(pool_size=3)(conv)  # default = 2
        conv = Flatten()(conv)

        attention_probs = Dense(conv._keras_shape[1], activation='softmax', name='dense_attention_probs_' + str(sub_i))(
            conv)
        attention_mul = Multiply(name='dense_attention_mul_' + str(sub_i))([conv, attention_probs])

        attention_mul = BatchNormalization()(attention_mul)  # batch normalization
        attention_mul = Activation(activate_f)(attention_mul)

        # attention 한 것과 아닌 것을 같이 넣기
        conv_blocks.append(conv)
        conv_blocks.append(attention_mul)

        sub_i += 1

    z = Concatenate()(conv_blocks) if len(conv_blocks) > 1 else conv_blocks[0]

    z = Dropout(dropout_prob[1])(z)
    z = Dense(int(hidden_dims),
              activation=activate_f,
              kernel_initializer='glorot_normal',
              bias_initializer='glorot_normal')(z)
    attention_probs = Dense(int(hidden_dims), activation='softmax',
                            kernel_initializer='glorot_normal',
                            bias_initializer='glorot_normal',
                            name='dense_post_attention_probs')(z)
    z = Multiply(name='dense_post_attention')([z, attention_probs])
    z = BatchNormalization()(z)  # batch normalization
    z = Activation(activate_f)(z)
    z = Dropout(dropout_prob[0])(z)

    # flat 변수와 결합
    # TODO: 테스트를 위해 임시적으로 생략
    z = Concatenate()([z, flat_dense])

    for deep_idx in range(0):
        z = Dense(int(hidden_dims),
                  activation=activate_f,
                  kernel_initializer='glorot_uniform',
                  bias_initializer='glorot_uniform')(z)
        '''        '''
        attention_probs = Dense(int(hidden_dims), activation='softmax',
                                kernel_initializer='glorot_normal',
                                bias_initializer='glorot_normal',
                                name='deep_attention_probs' + str(deep_idx))(z)
        z = Multiply(name='deep_attention' + str(deep_idx))([z, attention_probs])

        z = BatchNormalization()(z)  # batch normalization
        z = Activation(activate_f)(z)
        z = Dropout(dropout_prob[0])(z)

    model_output = Dense(3, activation='softmax',
                         kernel_initializer='glorot_normal',
                         bias_initializer='glorot_normal')(z)

    model = Model([model_input, flat_input], model_output)

    model.summary()

    return model


def create_rnn_model(input_shape, flat_input_shape, dropout_prob, hidden_dims, num_filters, filter_sizes):
    # flat
    flat_input = Input(shape=flat_input_shape)

    flat_dense = Dense(int(flat_input_shape[0]),
                       kernel_initializer='glorot_normal',
                       bias_initializer='glorot_normal',
                       name='flat_dense1')(flat_input)

    # '''
    # flat_dense = Dense(int(flat_input_shape[0]),
    #                         kernel_initializer='glorot_normal',
    #                         bias_initializer='glorot_normal',
    #                         name='flat_dense1')(flat_input)
    # flat_dense = LeakyReLU(alpha=0.3)(flat_dense)
    # flat_attention_probs = Dense(int(flat_input_shape[0]), activation='softmax',
    #                     kernel_initializer='glorot_normal',
    #                     bias_initializer='glorot_normal',
    #                     name='flat_dense_attention_probs')(flat_dense)
    #
    # flat_dense = Multiply(name='flat_dense_attention')([flat_dense, flat_attention_probs])
    # '''

    # time series
    model_input = Input(shape=input_shape)

    z = model_input

    '''           
    attention_mul = attention_3d_block(model_input)
    z = attention_mul
    '''

    # GRU Test (이것을 CNN의 초기 Input으로 보기)
    '''
    z = Dropout(dropout_prob[0])(z)
    z = Bidirectional(GRU(num_features
                   , return_sequences=True, recurrent_dropout=0.5
                   , activation='relu'))(z)
    '''

    z = Dropout(dropout_prob[0])(z)

    # LSTM
    # z = Bidirectional(CuDNNGRU(num_features
    #                       , return_sequences=False, recurrent_dropout=0.0
    #                       #                      , activation='tanh'))(z)
    #                       , activation='relu'))(z)

    z = CuDNNGRU(num_features
                 , return_sequences=True)(z)

    z = CuDNNGRU(num_features
                 , return_sequences=True)(z)

    z = CuDNNGRU(num_features
                 , return_sequences=False)(z)
    '''
    z = GRU(num_features
                 , return_sequences=True)(z)

    z = GRU(num_features
                 , return_sequences=True)(z)

    z = GRU(num_features
                 , return_sequences=False)(z)
    '''

    #           , activation='elu'))(z)

    z = Concatenate()([z, flat_dense])

    # z = Dropout(dropout_prob[1])(z)

    '''     '''
    # z = Dropout(dropout_prob[0])(z)

    '''
    attention_probs = Dense(int(z._keras_shape[1]), activation='softmax', 
                        kernel_initializer='glorot_normal',
                        bias_initializer='glorot_normal',
                        name='concat_post_attention_probs')(z)
    z = Multiply(name='concat_post_attention')([z, attention_probs])
    '''

    # for deep_idx in range(3):
    #     z = Dense(int(hidden_dims),
    #               activation='relu',
    #               kernel_initializer='glorot_uniform',
    #               bias_initializer='glorot_uniform')(z)
    #     '''        '''
    #     attention_probs = Dense(int(hidden_dims), activation='softmax',
    #                             kernel_initializer='glorot_normal',
    #                             bias_initializer='glorot_normal',
    #                             name='deep_attention_probs' + str(deep_idx))(z)
    #     z = Multiply(name='deep_attention' + str(deep_idx))([z, attention_probs])
    #
    #     z = BatchNormalization()(z)  # batch normalization
    #     z = Activation('relu')(z)
    #     z = Dropout(dropout_prob[0])(z)

    '''
    z_dim = int(z._keras_shape[1])

    z = Dense(int(z_dim/2), 
                activation="tanh",
                kernel_initializer='glorot_uniform',
                bias_initializer='glorot_uniform')(z)
    #z = LeakyReLU(alpha=0.3)(z)
    attention_probs = Dense(int(z_dim/2), activation='softmax', 
                            kernel_initializer='glorot_normal',
                            bias_initializer='glorot_normal',
                            name='deep_attention_probs1')(z)
    z = Multiply(name='deep_attention1')([z, attention_probs])

    z = Dropout(dropout_prob[0])(z)

    z = Dense(int(z_dim/3), 
                    activation="tanh",
                    kernel_initializer='glorot_uniform',
                    bias_initializer='glorot_uniform')(z)
    #z = LeakyReLU(alpha=0.3)(z)

    attention_probs = Dense(int(z_dim/3), activation='softmax', 
                            kernel_initializer='glorot_normal',
                            bias_initializer='glorot_normal',
                            name='deep_attention_probs2')(z)
    z = Multiply(name='deep_attention2')([z, attention_probs])

    z = Dropout(dropout_prob[0])(z)

    z = Dense(int(z_dim/4), 
              activation="tanh",
                    kernel_initializer='glorot_uniform',
                    bias_initializer='glorot_uniform')(z)
    #z = LeakyReLU(alpha=0.3)(z)

    attention_probs = Dense(int(z_dim/4), activation='softmax', 
                            kernel_initializer='glorot_normal',
                            bias_initializer='glorot_normal',
                            name='deep_attention_probs3')(z)
    z = Multiply(name='deep_attention3')([z, attention_probs])
    '''

    z = keras.layers.BatchNormalization()(z)  # batch normalization

    # model_output = Dense(6, activation='tanh',
    # model_output = Dense(2, activation='linear',
    #                      kernel_initializer='glorot_normal',
    #                      bias_initializer='glorot_normal')(z)

    model_output = Dense(3, activation='softmax',
                         kernel_initializer='glorot_normal',
                         bias_initializer='glorot_normal')(z)

    # model_output = LeakyReLU(alpha=0.3)(model_output)

    # model_output = PReLU(alpha_initializer='zeros', alpha_regularizer=None, alpha_constraint=None, shared_axes=None)(model_output)
    # model_output = ELU()(model_output)

    model = Model([model_input, flat_input], model_output)
    # model = Model(model_input, model_output)

    model.summary()

    return model


def reset_weights(model):
    session = K.get_session()

    init_op = tf.global_variables_initializer()
    session.run(init_op)

    for layer in model.layers:
        try:
            if hasattr(layer, 'kernel_initializer'):
                layer.kernel.initializer.run(session=session)
        except:
            print('(skip) no kernel init...')


def _prep_train(collection, source, symbol_pair, start_date, end_date, train_ratio=0.7, batch_size=512):
    # mongo data 가져오기
    train_data_path, line_cnt = mongo_utils.get_period_data(collection, source, symbol_pair, start_date,
                                                            end_date, limit=1000, write_file=True, service='excalibur')

    # train_data_path = train_data_path.replace('excalibur', 'excalibur-classifier')

    # shuffle data
    ''''''
    rand_path = train_data_path + '.rand'
    os.system('rm %s*' % (rand_path))  # rm exists
    # os.system('shuf %s > %s' % (train_data_path, rand_path) )
    os.system(
        "awk 'BEGIN{srand();} {printf \"%06d %s\\n\", rand()*1000000, $0;}' " + train_data_path + " | sort -n | cut -c8- > " + rand_path)

    # os.system('rm %s' % (train_data_path))  # rm
    # os.remove(train_data_path)

    # rand_path = train_data_path

    # split data
    # partitioning
    train_split_data_path = rand_path + '.train'
    valid_split_data_path = rand_path + '.valid'

    os.system('head -n %s %s > %s' % (str(int(line_cnt * train_ratio)), train_data_path, train_split_data_path))
    os.system(
        'tail -n %s %s > %s' % (str(line_cnt - int(line_cnt * train_ratio)), train_data_path, valid_split_data_path))

    # os.system('head -n %s %s > %s' % (str(int(line_cnt * train_ratio)), rand_path, train_split_data_path))
    # os.system('tail -n %s %s > %s' % (str(line_cnt - int(line_cnt * train_ratio)), rand_path, valid_split_data_path))

    # os.remove(rand_path)
    os.system('rm %s' % (rand_path))  # rm
    os.system('rm %s*%s*' % (rand_path, 'batch'))  # rm exists

    # batch 파일로 쪼개기
    train_split_batch_prefix = train_split_data_path + '.batch.'
    valid_split_batch_prefix = valid_split_data_path + '.batch.'
    os.system('split -l %s %s %s' % (str(batch_size), train_split_data_path, train_split_batch_prefix))
    os.system('split -l %s %s %s' % (str(batch_size), valid_split_data_path, valid_split_batch_prefix))

    # os.remove(train_split_data_path)
    # os.remove(valid_split_data_path)
    os.system('rm %s' % (train_split_data_path))  # rm
    os.system('rm %s' % (valid_split_data_path))  # rm

    # TODO: numpy array로 만들어서 직렬화하기 (속도 향상)

    # train_ids = object_ids[:int(len(object_ids) * train_ratio)]
    # valid_ids = object_ids[int(len(object_ids) * train_ratio):]

    # with open(train_data_path + '.rand', 'r') as fr:
    #     for line in fr:
    #         break

    return train_data_path


def train(collection, source, symbol_pair, start_date, end_date, max_len, filter_sizes, num_filters, dropout_prob,
          flat_num_features, num_features,
          hidden_dims, epochs, best_filepath, back_filepath, learning_rate=0.01, server=None, database=None):
    opt = optimizers.Adam(lr=learning_rate)
    # opt = optimizers.RMSprop()

    # decay_rate = learning_rate * 0.9 # 동적으로 감소하도록
    # opt = optimizers.Adam(lr=learning_rate, decay=decay_rate)

    input_shape = (max_len, num_features)
    flat_input_shape = (flat_num_features,)

    best_profit = None

    max_val = None

    # # total shuffle (전체 데이터 셔플)
    # # 한번만 하고 고정
    # index_shuf = list(range(len(y_train)))
    # random.shuffle(index_shuf)
    #
    # new_concat_xs = []
    # new_concat_flat_xs = []
    # new_concat_ys = []
    # for i in index_shuf:
    #     new_concat_xs.append(x_train[0][i])
    #     new_concat_flat_xs.append(x_train[1][i])
    #     new_concat_ys.append(y_train[i])
    #
    # del x_train
    # del y_train
    #
    # x_train = [new_concat_xs, new_concat_flat_xs]
    # # x_train = [np.array(new_concat_xs, dtype=np.float32)]
    # y_train = np.array(new_concat_ys)

    #
    train_ratio = 0.7
    batch_size = 512
    train_data_path = _prep_train(collection, source, symbol_pair, start_date, end_date, train_ratio=train_ratio,
                                  batch_size=batch_size)

    # temp..
    # train_data_path = '/home/rtm/rtm-base//train/excalibur-bitmex-BTCUSD-20170101000000-20181130235900.json'

    # exit()
    #
    # object_ids = mongo_utils.get_period_ids(collection, source, symbol_pair, start_date, end_date,
    #                                               limit=1000)
    #
    # # randomize
    # shuffle(object_ids)
    #
    # # Parameters
    # params = {'batch_size': 512,
    #           'collection': collection.name,
    #           'source': source,
    #           'symbol_pair': symbol_pair,
    #           'database': database,
    #           'server': server,
    #           }
    #
    # # partitioning
    # train_ratio = 0.7
    # train_ids = object_ids[:int(len(object_ids) * train_ratio)]
    # valid_ids = object_ids[int(len(object_ids) * train_ratio):]

    # file based
    training_generator = DataSequence(train_data_path + '.rand.train.batch')
    validation_generator = DataSequence(train_data_path + '.rand.valid.batch')
    # training_generator = DataSequence(train_data_path + '.train.batch')
    # validation_generator = DataSequence(train_data_path + '.valid.batch')

    # memory based
    # train_timeseries_xs, train_flat_xs, train_ys = _load_data(train_data_path + '.rand.train')
    # valid_timeseries_xs, valid_flat_xs, valid_ys = _load_data(train_data_path + '.rand.valid')

    # for i in range(epochs):
    for i in range(1):
        print('iteration=', (i + 1))

        # Training
        print('learning_rate = ', learning_rate)

        # model = create_model(input_shape, flat_input_shape, dropout_prob, hidden_dims, num_filters, filter_sizes)
        model = create_rnn_model(input_shape, flat_input_shape, dropout_prob, hidden_dims, num_filters,
                                 filter_sizes)  # RNN

        # checkpoint = ModelCheckpoint(back_filepath, monitor='val_categorical_accuracy', verbose=1, save_best_only=True,
        #                              mode='max')
        checkpoint = ModelCheckpoint(back_filepath, monitor='val_precision', verbose=1, save_best_only=True,
                                     mode='max')
        # checkpoint = ModelCheckpoint(back_filepath, monitor='val_rmse', verbose=1, save_best_only=True,
        #                              mode='min')
        ealrystop = EarlyStopping(monitor='val_loss', min_delta=0.0, patience=0, verbose=0, mode='auto', baseline=None)

        # callbacks_list = [checkpoint, ealrystop]
        callbacks_list = [checkpoint]
        # model.compile(loss='categorical_crossentropy',
        model.compile(loss='binary_crossentropy',
                      optimizer=opt,
                      metrics=['categorical_accuracy', precision, recall, f1score])

        # 가중치 초기화

        reset_weights(model)

        # class_weight = {0: 0.1, 1: 0.45, 2: 0.45}
        class_weight = {0: 1., 1: 5., 2: 5.}

        history = model.fit_generator(generator=training_generator, validation_data=validation_generator,
                                      shuffle=True,
                                      epochs=epochs,
                                      # class_weight=class_weight,
                                      # use_multiprocessing=True,
                                      # workers=8,
                                      callbacks=callbacks_list)
        '''


        history = model.fit([train_timeseries_xs, train_flat_xs], train_ys, validation_data=[[valid_timeseries_xs, valid_flat_xs], valid_ys],
        #history = model.fit(train_timeseries_xs, train_ys, validation_data=[valid_timeseries_xs, valid_ys],
                            batch_size=512, 
                            epochs=epochs, 
                            #class_weight=class_weight,
                            callbacks=callbacks_list,
                            shuffle=True)  # starts training
        '''

        # 이전 보다 향상되었으면 모델 갱신
        # sub_max_val = max(history.history['val_cosine_sim'])
        # sub_max_val = max(history.history['val_categorical_accuracy'])
        sub_max_val = max(history.history['val_precision'])

        if max_val is None or sub_max_val > max_val:
            # if max_val is None or sub_max_val < max_val:
            # Best model 불러오기
            model = load_model(back_filepath,
                               custom_objects={'precision': precision, 'recall': recall, 'f1score': f1score})
            # save best model
            model.save(best_filepath)
            print('improved from ', max_val, ' to ', sub_max_val)

            # max_val 갱신
            max_val = sub_max_val

        del model


if __name__ == '__main__':

    filename = '/home/rtm/jupyter/data/GD/2019040805'

    print(filename)

    # read pre-processed data

    feature_set, feature_name_set, max_ratio_list, min_ratio_list = preprocess(filename)

    #
    #
    # # Get parameters
    # argv = sys.argv
    # print(argv)
    #
    # if len(argv) < 3:
    #     settings.LOGGER.info('usage: python gd_trainer.py [start_date] [end_date]')
    #     exit()
    #
    # start_date = argv[1]
    # end_date = argv[2]
    #
    # if start_date == '-1' and end_date == '-1':
    #     # yesterday로 setting
    #     utc_time = time.mktime(time.gmtime())
    #     start_date = str((datetime.datetime.fromtimestamp(utc_time) - datetime.timedelta(days=1)).strftime('%Y%m%d'))
    #     end_date = start_date
    #
    # start_date = datetime.datetime(int(start_date[:4]), int(start_date[4:6]), int(start_date[6:8]), 0, 0)
    # end_date = datetime.datetime(int(end_date[:4]), int(end_date[4:6]), int(end_date[6:8]), 23, 59)
    #
    # config = tf.ConfigProto()
    # config.gpu_options.per_process_gpu_memory_fraction = 0.5
    #
    # # Initialize all variables
    # sess = tf.Session(config=config)
    # set_session(sess)
    #
    # K.tensorflow_backend.manual_variable_initialization(False)
    #
    # try:
    #     client = MongoClient('localhost', port=14350, username='rtmadmin', password='Rocket!Go!go@')
    # except:
    #     client = MongoClient('gpu.rtm.ai', port=14350, username='rtmadmin', password='Rocket!Go!go@')
    #
    # mongo_db = client['arp']
    #
    # service = 'gd'
    # version = '0.5'
    # # source = 'bitmex'
    # source = 'upbit'
    # module = 'preprocessor'
    # # collection = mongo_utils.get_mongo_collection(mongo_db, service, version, source, module, others=['test'])
    # collection = mongo_utils.get_mongo_collection(mongo_db, service, version, source, module, others=[])
    #
    # max_len = 60 * 24
    #
    # # CNN
    # filter_sizes = (5, 15, 30, 60, 60 * 3, 60 * 6, 60 * 12)
    # num_filters = 10
    #
    # # curr. best
    # dropout_prob = (0.2, 0.8)
    # flat_num_features = 3
    # num_features = 5
    # hidden_dims = 50
    # epochs = 50
    #
    # best_filepath = settings.BASE_PATH + 'model/%s-%s-classifier-best.hdf5' % (service, source)
    # back_filepath = settings.BASE_PATH + 'model/%s-%s-classifier-back.hdf5' % (service, source)
    #
    # if not os.path.exists(settings.BASE_PATH + 'model'):
    #     # 폴더가 없으면 만들기
    #     settings.LOGGER.info('[INFO] make model dir: %s', settings.BASE_PATH + 'model')
    #     os.makedirs(settings.BASE_PATH + 'model')
    #
    # # training
    # train(collection, source, symbol_pair, start_date, end_date, max_len, filter_sizes, num_filters, dropout_prob,
    #       flat_num_features, num_features,
    #       hidden_dims, epochs, best_filepath, back_filepath, server='gpu.rtm.ai', database='arp')