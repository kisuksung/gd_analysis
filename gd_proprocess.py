import numpy as np
import pandas as pd
import json
import sys
import settings
import datetime
import time
#from datetime import datetime, timedelta
import os

def preprocess(filename):

    #filename = './2019040805'
    feature_names = ['bn_trade_cnt', 'bm_trade_max_delay', 'ex_spread_d1', 'bm_ob_mid', 'bn_ob_mid', 'curr_ex_spread',
                     'bm_ob_ask_10', 'bm_ob_bid_10', 'bn_ob_ask_10', 'bn_ob_bid_10', 'bn_trade_sell_cnt',
                     'bn_trade_sell_amt', 'bn_trade_buy_cnt', 'bn_trade_buy_amt', 'bn_trade_avg_delay',
                     'bn_trade_max_delay', 'bm_trade_sell_cnt', 'bm_trade_sell_amt', 'bm_trade_buy_cnt',
                     'bm_trade_buy_amt', 'bm_trade_avg_delay', 'bm_trade_max_delay_n', 'timestamp', 'prediction_result']    # json_data = open(filename).read()

    # 데이터 읽기
    feature_set = []
    feature_name_set = []
    with open(filename) as json_file:
        # doc = json.loads(json_file.read())
        # print(doc)
        on_off = 0
        for line in json_file:
            doc = json.loads(line)
            # feature_set.append(doc)
            feature_set.append([v for k, v in doc.items()])
            if on_off == 0:
                feature_name_set.append([k for k, v in doc.items()])
                on_off = 1
            # break
    feature_name_set = feature_name_set[0]
    print(feature_name_set)
    # 관심지표 만들기
    # max_list: 2, 10, 30, 60, 180 초 ask max 값 (0~0.5초는 제외)
    # min_list: 2, 10, 30, 60, 180 초 bid max 값 (0~0.5초는 제외)
    # mid_p_list: 2, 10, 30, 60, 180 초 ask/bid 평균값 (전후 0.1초구간 측정)
    # mid_p_0_list: 현재 시점 ask/bid 평균값
    time_idx = feature_name_set.index('timestamp')
    feature_set = sorted(feature_set, key=lambda x: x[time_idx])
    # feature_set = feature_set[0:10000]
    target_cd = []
    target_reg = []
    price_period1 = []
    price_period2 = []
    price_period3 = []
    price_period4 = []
    pr0_sec = 0.5
    pr1_sec = 2
    pr2_sec = 10
    pr3_sec = 30
    pr4_sec = 60
    pr5_sec = 180
    price_idx = feature_name_set.index('bm_ob_mid')
    price_max_idx = feature_name_set.index('bm_ob_ask_10')
    price_min_idx = feature_name_set.index('bm_ob_bid_10')
    max_list = []
    min_list = []
    mid_p_list = []
    mid_p_0_list = []
    for i in range(0, len(feature_set)):
        t_0 = feature_set[i][time_idx]
        mid_p_0 = feature_set[i][price_idx]

        check_2sec = False
        check_10sec = False
        check_30sec = False
        check_60sec = False
        check_180sec = False
        temp_max = []
        temp_min = []
        temp_mid_p = []
        max_by_t = []
        min_by_t = []
        mid_p_by_t = []
        if t_0 + pr5_sec >= feature_set[-1][time_idx]:
            print("reading process completed")
            print(feature_set[i][time_idx])
            print(feature_set[-1][time_idx])
            break

        for j in range(i, len(feature_set)):

            if feature_set[j][time_idx] < t_0 + pr0_sec:  # 0.5초 이내 일때
                continue
            elif feature_set[j][time_idx] < t_0 + pr1_sec - 0.1:  # 0.5초 ~ 2 - 0.1초 이내 일때
                temp_max.append(feature_set[j][price_max_idx][0][0])
                temp_min.append(feature_set[j][price_min_idx][0][0])
            elif feature_set[j][time_idx] < t_0 + pr1_sec + 0.1:  # 2 - 0.1초 ~ 2 + 0.1초 이내 일때
                temp_max.append(feature_set[j][price_max_idx][0][0])
                temp_min.append(feature_set[j][price_min_idx][0][0])
                temp_mid_p.append(feature_set[j][price_idx])
                # print(feature_set[j][price_idx])
            elif feature_set[j][time_idx] < t_0 + pr2_sec - 0.1:  # 2 + 0.1초 ~ 10 - 0.1초 이내 일때
                if check_2sec is False:
                    # print(temp_mid_p)
                    if len(temp_mid_p) == 0:
                        mid_p_by_t.append(feature_set[j][price_idx])
                    else:
                        mid_p_by_t.append(np.mean(temp_mid_p))
                    max_by_t.append(np.max(temp_max))
                    min_by_t.append(np.min(temp_min))
                    temp_mid_p = []
                    check_2sec = True
                temp_max.append(feature_set[j][price_max_idx][0][0])
                temp_min.append(feature_set[j][price_min_idx][0][0])
            elif feature_set[j][time_idx] < t_0 + pr2_sec + 0.1:  # 10 - 0.1초 ~ 10 + 0.1초 이내 일때
                temp_max.append(feature_set[j][price_max_idx][0][0])
                temp_min.append(feature_set[j][price_min_idx][0][0])
                temp_mid_p.append(feature_set[j][price_idx])
            elif feature_set[j][time_idx] < t_0 + pr3_sec - 0.1:  # 10 + 0.1초 ~ 30 - 0.1초 이내 일때
                if check_10sec is False:
                    if len(temp_mid_p) == 0:
                        mid_p_by_t.append(feature_set[j][price_idx])
                    else:
                        mid_p_by_t.append(np.mean(temp_mid_p))
                    max_by_t.append(np.max(temp_max))
                    min_by_t.append(np.min(temp_min))
                    temp_mid_p = []
                    check_10sec = True
                temp_max.append(feature_set[j][price_max_idx][0][0])
                temp_min.append(feature_set[j][price_min_idx][0][0])
            elif feature_set[j][time_idx] < t_0 + pr3_sec + 0.1:  # 30 - 0.1초 ~ 30 + 0.1초 이내 일때
                temp_max.append(feature_set[j][price_max_idx][0][0])
                temp_min.append(feature_set[j][price_min_idx][0][0])
                temp_mid_p.append(feature_set[j][price_idx])
            elif feature_set[j][time_idx] < t_0 + pr4_sec - 0.1:  # 30 + 0.1초 ~ 60 - 0.1초 이내 일때
                if check_30sec is False:
                    if len(temp_mid_p) == 0:
                        mid_p_by_t.append(feature_set[j][price_idx])
                    else:
                        mid_p_by_t.append(np.mean(temp_mid_p))
                    max_by_t.append(np.max(temp_max))
                    min_by_t.append(np.min(temp_min))
                    temp_mid_p = []
                    check_30sec = True
                temp_max.append(feature_set[j][price_max_idx][0][0])
                temp_min.append(feature_set[j][price_min_idx][0][0])
            elif feature_set[j][time_idx] < t_0 + pr4_sec + 0.1:  # 60 - 0.1초 ~ 60 + 0.1초 이내 일때
                temp_max.append(feature_set[j][price_max_idx][0][0])
                temp_min.append(feature_set[j][price_min_idx][0][0])
                temp_mid_p.append(feature_set[j][price_idx])
            elif feature_set[j][time_idx] < t_0 + pr5_sec - 0.1:  # 60 + 0.1초 ~ 180 - 0.1초 이내 일때
                if check_60sec is False:
                    if len(temp_mid_p) == 0:
                        mid_p_by_t.append(feature_set[j][price_idx])
                    else:
                        mid_p_by_t.append(np.mean(temp_mid_p))
                    max_by_t.append(np.max(temp_max))
                    min_by_t.append(np.min(temp_min))
                    temp_mid_p = []
                    check_60sec = True
                temp_max.append(feature_set[j][price_max_idx][0][0])
                temp_min.append(feature_set[j][price_min_idx][0][0])
            elif feature_set[j][time_idx] < t_0 + pr5_sec + 0.1:  # 180 - 0.1초 ~ 180 + 0.1초 이내 일때
                temp_max.append(feature_set[j][price_max_idx][0][0])
                temp_min.append(feature_set[j][price_min_idx][0][0])
                temp_mid_p.append(feature_set[j][price_idx])
            else:
                if check_180sec is False:
                    if len(temp_mid_p) == 0:
                        mid_p_by_t.append(feature_set[j][price_idx])
                    else:
                        mid_p_by_t.append(np.mean(temp_mid_p))
                    max_by_t.append(np.max(temp_max))
                    min_by_t.append(np.min(temp_min))
                    temp_mid_p = []
                    check_180sec = True
                break
        #     if i is not 0 and i % 1000 == 0:
        #         print(i, feature_set[i][time_idx])
        #         #break
        max_list.append(max_by_t)
        min_list.append(min_by_t)
        mid_p_list.append(mid_p_by_t)
        mid_p_0_list.append(mid_p_0)

    # 지표만들기

    max_ratio_list = []  # 2,10,30,60,180초간 max/현재price 변화율
    min_ratio_list = []  # 2,10,30,60,180초간 min/현재price 변화율

    # print(len(mid_p_0_list))
    # print(len(mid_p_list))
    for i in range(0, len(mid_p_0_list)):
        max_ratio_list.append(np.divide(np.array(max_list[i]) - feature_set[i][price_max_idx][0][0],
                                        feature_set[i][price_max_idx][0][0]))
        min_ratio_list.append(np.divide(np.array(min_list[i]) - feature_set[i][price_min_idx][0][0],
                                        feature_set[i][price_min_idx][0][0]))

    print(max_ratio_list[0])
    print(max_ratio_list[-1])
    print(min_ratio_list[0])
    print(min_ratio_list[-1])

    # output_name = "./indicesForTarget.txt"
    #
    # with open(output_name, "w") as text_file:
    #     # text_file.write(output_name)
    #     text_file.write("sample num \t mid_p_0 \t max_ratio_list \t\t\t\t\t min_ratio_list \n")
    #     for i in range(0, len(mid_p_0_list)):
    #
    #         text_file.write("%.8f\t" % feature_set[i][time_idx])
    #         text_file.write("%.4f\t" % mid_p_0_list[i])
    #         for j in range(0, len(max_ratio_list[i])):
    #             text_file.write("%.6f\t" % max_ratio_list[i][j])
    #         for j in range(0, len(min_ratio_list[i])):
    #             text_file.write("%.6f\t" % min_ratio_list[i][j])
    #         text_file.write("\n")
    return feature_set, feature_name_set, max_ratio_list, min_ratio_list


if __name__ == '__main__':
    # filename = '/home/rtm/jupyter/data/GD/2019040805'
    # prep = preprocess(filename)

    # Get parameters
    argv = sys.argv
    print(argv)

    if len(argv) < 3:
        settings.LOGGER.info('usage: python gd_preprocess.py [start_date] [end_date]')
        exit()

    start_date = argv[1]
    end_date = argv[2]

    if start_date == '-1' and end_date == '-1':
        # yesterday로 setting
        utc_time = time.mktime(time.gmtime())
        start_date = str((datetime.datetime.fromtimestamp(utc_time) - datetime.timedelta(days=1)).strftime('%Y%m%d'))
        end_date = start_date

    print(start_date, end_date)

    service = 'gd_analyzer'
    version = '0.5'
    # source = 'bitmex'
    source = 'bitmex'
    module = 'preprocessor'

    # input_collection = mongo_db['socket_scalping_ticker']
    # # output_collection = mongo_utils.get_mongo_collection(mongo_db, service, version, source, module, others=['test'])
    # output_collection = mongo_utils.get_mongo_collection(mongo_db, service, version, source, module, others=[])

    # sampling_ratio = 0.1 # 10%만 취하기
    sampling_ratio = None  # 전수
    start_date = datetime.datetime(int(start_date[:4]), int(start_date[4:6]), int(start_date[6:8]), 0, 0)
    end_date = datetime.datetime(int(end_date[:4]), int(end_date[4:6]), int(end_date[6:8]), 23, 59)


    sub_start_date = start_date
    sub_buffer_date = sub_start_date - datetime.timedelta(days=9)
    sub_end_date = sub_start_date + datetime.timedelta(weeks=4)
    sub_end_date = sub_end_date.replace(hour=23, minute=59, second=59)

    # while True:
    while sub_start_date < end_date:

        if sub_end_date > end_date:
            sub_end_date = end_date

        print(sub_buffer_date, sub_start_date, sub_end_date)

        # preprocess
        preprocess(sub_buffer_date, sub_start_date, sub_end_date)

        sub_start_date += datetime.timedelta(weeks=4, days=1)
        # sub_start_date = sub_start_date.replace(hour=23, minute=0, second=0)
        sub_buffer_date = sub_start_date - datetime.timedelta(days=9)
        sub_end_date = sub_start_date + datetime.timedelta(weeks=4)
        sub_end_date = sub_end_date.replace(hour=23, minute=59, second=59)
